var path = require('path');
var webpack = require("webpack");
var pkg = require('./package.json');

module.exports = {
    entry: './app/app.js',
    output: {
        filename: pkg.name + '.js',
        path: path.resolve(__dirname, 'dist'),
        publicPath: "/dist",
        libraryTarget: 'umd'
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'ng-annotate-loader!babel-loader'
            },
            {
                test: /\.html$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'raw-loader'
            },
            {
                test: /\.css$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'style-loader!css-loader'
            }
        ]
    }
};