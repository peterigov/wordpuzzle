import angular from 'angular';
import PlayersModule from './players/players';
import ContentModule from './content/content';
import TimeModule from './content/time/time';
import ScoreModule from './content/score/score';
import WordModule from './content/word/word';
import NewGameModule from './content/new-game/new-game';
import GameResultModule from './content/game-result/game-result';

const ComponentsModule = angular.module('components', [
    PlayersModule.name,
    ContentModule.name,
    TimeModule.name,
    ScoreModule.name,
    WordModule.name,
    NewGameModule.name,
    GameResultModule.name
]);

export default ComponentsModule;