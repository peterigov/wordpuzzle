import template from './players.tmpl.html';
import controller from './players.controller';
import './players.css';

const playersComponent = {
    template,
    controller,
    controllerAs: 'playersCtrl'
};

export default playersComponent;