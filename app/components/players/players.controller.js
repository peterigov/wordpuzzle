class PlayersController {
    constructor(PlayersService) {
        'ngInject';

        this.PlayersService = PlayersService;
    }

    $onInit() {
        this.PlayersService.getPlayers().then(players => {
            this.players = players;
        });
    }

    showLoader() {
        return !this.players;
    }
}

PlayersController.$inject = ['PlayersService'];

export default PlayersController;