import template from './content.tmpl.html';
import controller from './content.controller';
import './content.css';

const contentComponent = {
    template,
    controller,
    controllerAs: 'contentCtrl'
};

export default contentComponent;