import angular from 'angular';
import contentComponent from './content.component';

const ContentModule = angular.module('content', [])
    .component('content', contentComponent);

export default ContentModule;