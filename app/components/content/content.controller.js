class ContentController {
    constructor(GameModel) {
        'ngInject';

        this.GameModel = GameModel;

    }

    showNewGameModal() {
        return !this.GameModel.isInGame && this.GameModel.gameResult === null;
    }

    showGameResultModal() {
        return this.GameModel.gameResult !== null;
    }
}

ContentController.$inject = ['GameModel'];

ContentController.$$ngIsClass = true;

export default ContentController;