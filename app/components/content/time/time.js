import angular from 'angular';
import timeComponent from './time.component';

const TimeModule = angular.module('time', [])
    .component('time', timeComponent);

export default TimeModule;