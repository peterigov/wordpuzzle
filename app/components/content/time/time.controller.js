class TimeController {
    constructor(GameModel) {
        'ngInject';

        this.GameModel = GameModel;
    }

    $onInit() {
        this.getTimeLeft = this.GameModel.getTimeLeft.bind(this.GameModel);
    }
}

TimeController.$inject = ['GameModel'];

TimeController.$$ngIsClass = true;

export default TimeController;