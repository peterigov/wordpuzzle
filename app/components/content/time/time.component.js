import template from './time.tmpl.html';
import controller from './time.controller';
import './time.css';

const timeComponent = {
    template,
    controller,
    controllerAs: 'timeCtrl'
};

export default timeComponent;