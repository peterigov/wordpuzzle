class ScoreController {
    constructor(GameModel) {
        'ngInject';

        this.GameModel = GameModel;
    }

    $onInit() {
        this.getScore = this.GameModel.getScore.bind(this.GameModel);
    }
}

ScoreController.$inject = ['GameModel'];

ScoreController.$$ngIsClass = true;

export default ScoreController;