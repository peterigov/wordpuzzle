import angular from 'angular';
import scoreComponent from './score.component';

const ScoreModule = angular.module('score', [])
    .component('score', scoreComponent);

export default ScoreModule;