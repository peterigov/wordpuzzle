import template from './score.tmpl.html';
import controller from './score.controller';
import './score.css';

const scoreComponent = {
    template,
    controller,
    controllerAs: 'scoreCtrl'
};

export default scoreComponent;