import angular from 'angular';
import gameResultComponent from './game-result.component';

const GameResultModule = angular.module('gameResult', [])
    .component('gameResult', gameResultComponent);

export default GameResultModule;