class GameResultController {
    constructor($scope, GameModel, PlayersService) {
        'ngInject';

        this.$scope = $scope;
        this.GameModel = GameModel;
        this.PlayersService = PlayersService;

        this.submitted = false;
    }

    $onInit() {
        this.getGameResult = this.GameModel.getGameResult.bind(this.GameModel);
    }

    onRestartClick() {
        this.GameModel.startGame();
    }

    submitResult() {
        const name = this.$scope.playerName;
        const score = this.getGameResult();

        if (!name) {
            return;
        }

        this.PlayersService.addPlayer({name, score});

        this.submitted = true;
    }

    submitInputKeyUp(e) {
        if (e.which === 13) {
            this.submitResult();
        }
    }
}

GameResultController.$inject = ['$scope', 'GameModel', 'PlayersService'];

GameResultController.$$ngIsClass = true;

export default GameResultController;