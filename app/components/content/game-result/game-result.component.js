import template from './game-result.tmpl.html';
import controller from './game-result.controller';
import './game-result.css';

const gameResultComponent = {
    template,
    controller,
    controllerAs: 'gameResultCtrl'
};

export default gameResultComponent;