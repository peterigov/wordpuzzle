import angular from 'angular';
import wordComponent from './word.component';

const WordModule = angular.module('word', [])
    .component('word', wordComponent);

export default WordModule;