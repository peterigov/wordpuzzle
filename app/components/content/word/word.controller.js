class WordController {
    constructor($scope, WordsModel, GameModel) {
        'ngInject';

        this.WordsModel = WordsModel;
        this.GameModel = GameModel;
        this.$scope = $scope;
    }

    $onInit() {
        this.getCurrentWord = this.WordsModel.getCurrentWord.bind(this.WordsModel);
    }

    onInputKeyUp(e) {
        const currentWord = this.getCurrentWord();

        if (e.which === 8 || e.which === 46) {
            if (currentWord.maxScore) {
                currentWord.maxScore--;
            }
        }

        if (this.$scope.input.toLocaleLowerCase() === currentWord.word.toLocaleLowerCase()) {
            this.GameModel.addScore(currentWord.maxScore);
            this.WordsModel.setCurrentWord();
            this.$scope.input = '';
        }
    }
}

WordController.$inject = ['$scope', 'WordsModel', 'GameModel'];

WordController.$$ngIsClass = true;

export default WordController;