import template from './word.tmpl.html';
import controller from './word.controller';
import './word.css';

const wordComponent = {
    template,
    controller,
    controllerAs: 'wordCtrl'
};

export default wordComponent;