import angular from 'angular';
import newGameComponent from './new-game.component';

const NewGameModule = angular.module('newGame', [])
    .component('newGame', newGameComponent);

export default NewGameModule;