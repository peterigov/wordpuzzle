import template from './new-game.tmpl.html';
import controller from './new-game.controller';
import './new-game.css';

const newGameComponent = {
    template,
    controller,
    controllerAs: 'newGameCtrl'
};

export default newGameComponent;