class NewGameController {
    constructor(GameModel) {
        'ngInject';

        this.GameModel = GameModel;
    }

    onStartButtonClick() {
        this.GameModel.startGame();
    }
}

NewGameController.$inject = ['GameModel'];

NewGameController.$$ngIsClass = true;

export default NewGameController;