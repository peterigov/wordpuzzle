import angular from 'angular';
import app from './app.component';
import CommonModule from './common/common';
import ComponentsModule from './components/components';

angular.module('app', [
    CommonModule.name,
    ComponentsModule.name
]).component('app', app);

document.body.removeChild(document.getElementById('loader'));