import template from './app.tmpl.html';
import './app.css';

const app = {
    template
};

export default app;