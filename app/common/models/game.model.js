let gameInterval;

class GameModel {
    constructor($q, $interval, WordsModel) {
        'ngInject';

        this.$q = $q;
        this.$interval = $interval;
        this.WordsModel = WordsModel;

        this.resetGameProps();
    }

    resetGameProps() {
        this.isInGame = false;
        this.gameResult = null;
        this.timeLeft = 40;
        this.score = 0;
    }

    getScore() {
        return this.score;
    }

    getTimeLeft() {
        return this.timeLeft;
    }

    getGameResult() {
        return this.gameResult;
    }

    addScore(score) {
        this.score += score;
    }

    startGame() {
        this.resetGameProps();

        this.isInGame = true;

        this.WordsModel.setCurrentWord();

        gameInterval = this.$interval(() => {
            this.timeLeft--;

            if (!this.timeLeft) {
                this.onGameEnd();
            }
        }, 1000);
    }

    onGameEnd() {
        this.$interval.cancel(gameInterval);

        this.isInGame = false;
        this.gameResult = this.score;
    }
}

GameModel.$inject = ['$q', '$interval', 'WordsModel'];

GameModel.$$ngIsClass = true

export default GameModel;