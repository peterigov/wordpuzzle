class WordsModel {
    constructor() {
        this.currentWord = null;

        this.ref = firebase.database().ref('words');

        this.loadWords();
    }

    loadWords() {
        this.ref.once('value').then(words => {
            this.words = words.val();
        });
    }

    getRandomInt(max) {
        return Math.floor(Math.random() * max);
    }

    shuffleWord(word) {
        let arr = word.split('');
        let length = word.length;

        for (let i = length - 1; i > 0; i--) {
            let j = this.getRandomInt(i);

            let tmp = arr[i];
            arr[i] = arr[j];
            arr[j] = tmp;
        }

        return arr.join('');
    }

    setCurrentWord() {
        const index = this.getRandomInt(this.words.length);

        const word = this.words[index];
        const shuffled = this.shuffleWord(word);
        const maxScore = Math.floor(Math.pow(1.95, word.length / 3));

        this.currentWord = {
            word,
            shuffled,
            maxScore
        };
    }

    getCurrentWord() {
        return this.currentWord;
    }
}

WordsModel.$$ngIsClass = true;

export default WordsModel;