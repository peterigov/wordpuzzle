class PlayersService {
    constructor($q) {
        'ngInject';

        this.$q = $q;
        this.ref = firebase.database().ref('players');
        this.numberOfPlayers = 0;
    }

    getPlayers() {
        const defer = this.$q.defer();

        this.ref.once('value').then(snapshot => {
            this.numberOfPlayers = snapshot.val().length;

            defer.resolve(snapshot.val());
        });

        return defer.promise;
    }

    addPlayer(player) {
        firebase.database().ref(`players/${this.numberOfPlayers}`).set(player);
    }
}

PlayersService.$inject = ['$q'];

PlayersService.$$ngIsClass = true;

export default PlayersService;