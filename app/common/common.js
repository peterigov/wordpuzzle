import angular from 'angular';
import WordsModel from './models/words.model';
import GameModel from './models/game.model';
import PlayersService from './services/players.service';

const CommonModule = angular.module('common', [])
    .service('WordsModel', WordsModel)
    .service('GameModel', GameModel)
    .factory('PlayersService', PlayersService);

export default CommonModule;